package com.luv2code.servletdemo;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "MvcDemoServlet")
public class MvcDemoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Step 0: Add data
        String[] students = {"Susan", "Suzuki", "Mercedes", "KISS"};
        request.setAttribute("student_list", students);

        // Step 1: get request dispatcher
        RequestDispatcher dispatcher =
                request.getRequestDispatcher("/view_students.jsp");

        // Step 2: forward the request to JSP
        dispatcher.forward(request, response);
    }
}
